package com.omics.conferencesseries.models;

public class VideoResponse {
    private String success;
    public VideoResponse(String success) {
        this.success = success;
    }
    public String getSuccess() {
        return success;
    }
}
