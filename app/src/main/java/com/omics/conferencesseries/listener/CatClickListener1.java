package com.omics.conferencesseries.listener;

import com.omics.conferencesseries.models.Categories;

public interface CatClickListener1 {

    void onAddClick(int position, Categories categories);
    void onRemoveClick(int position, Categories categories);

}
